//Now let's determine the size of the plots
var margin = {t:200,r:200,b:200,l:200},
    width = $('.canvas').width() - margin.l - margin.r,
    height = $('.canvas').height() - margin.t - margin.b;

//Set up SVG drawing elements
var svg = d3.select('.canvas')
    .append('svg')
    .attr('width', width + margin.l + margin.r)
    .attr('height', height + margin.t + margin.b)
    .append('g')
    .attr('transform','translate('+margin.l+','+margin.t+')');

//Load data from an external file *asynchronously*
d3.csv(
    'data/world_bank_2013.csv',
    function(d){
        return {
          country:d.country,
          gdp: +d.GDP,
          gdpPerCap: +d.GDP_per_capita,
          internetUser: +d.internet_users_per_100
        };
    },
    function(error,rows){
        //Curious to see what "rows" is?
        console.log(rows);

        //with data contained in array form, we can now draw
        draw(rows);
    });

function draw(rows){
    //we want to put gdp_per_cap on the x axis, and internet_user on the y axis
    //what how do we go from numbers to screen coordinates?
    //for now, we'll have to find out manually
    var internetUserMax = 100, gdpPerCapMax = 40000;

    svg.selectAll('country')
        .data(rows)
        .enter()
        .append('circle')
        .attr('class','country')
        .attr('cx', function(d){
            return d.internetUser / internetUserMax * width;
        })
        .attr('cy', function(d){
            return height - d.gdpPerCap / gdpPerCapMax * height
        })
        .attr('r',4)
        .append('title')
        .text(function(d){
            return d.country + ": " + d.internetUser + " users per 100 people";
        });
};