//Now let's determine the size of the plots
var margin = {t:200,r:200,b:300,l:200},
    width = $('.canvas').width() - margin.l - margin.r,
    height = $('.canvas').height() - margin.t - margin.b;

//Set up SVG drawing elements
var svg = d3.select('.canvas')
    .append('svg')
    .attr('width', width + margin.l + margin.r)
    .attr('height', height + margin.t + margin.b)
    .append('g')
    .attr('transform','translate('+margin.l+','+margin.t+')');

//Now we have load in data
//...



//once loaded, we call another function to draw this data
//...
function draw(data){

};