
//set up margins and dimensions
var pageWidth = $('.canvas').width(),
    pageHeight = $('.canvas').height();
var margin = {top: 100, right:100, bottom:200, left:100}; 
var plotWidth = pageWidth - margin.left - margin.right,
    plotHeight = pageHeight - margin.top - margin.bottom;

var data = [];

generateData();
drawPlot(); 


function generateData(){	 	
	for (var i = 0; i < 100; i ++){
		var xVal = i; 
		var yVal = Math.random()*plotHeight;
		data.push({x:xVal, y:yVal});
	}
}

function drawPlot(){

	var scatterplot = d3.select('.canvas')
		.append('svg')
		.attr('width', pageWidth)
		.attr('height', pageHeight)
		.append('g')
		.attr('class', 'scatter-plot')
		.attr('transform', 'translate(100, 100)'); 
		; 
	 var xScaler = d3.scale.linear().domain([0, data.length]).range([0, plotWidth]);
	 var yScaler = d3.scale.linear().domain([0, plotHeight]).range([0, plotHeight]);
	 
	scatterplot.selectAll('point')
	    .data(data)
	    .enter()
	    .append('circle')
	    .attr('class','point')
	    .attr('cx', function(d){  
	        return xScaler(d.x);
	    })
	    .attr('cy', function(d){
	       return yScaler(d.y);
	    })
	    .attr('r', 4)
	    ;
}