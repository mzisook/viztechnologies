$('#section-1 .alert').on('click',function(e){
    e.preventDefault();
    var message = $(this).html();
    alert(message);
});

$('#section-1 .green').on('click',function(e){
    //how to add a div to div#section-1?
    //consult this http://api.jquery.com/append/
    $(".wrapper").append("<div id='green'>...</div>"); 
    
});

$('#section-1 .blue').on('click',function(e){
    //how to add a div to div#section-1?
    //consult this http://api.jquery.com/append/
    $(".wrapper").append("<div id='blue'>...</div>"); 
    
});

$('#section-2').on('click',function(e){
	console.log("clicked on color-transition"); 
    //how to add a div to div#section-1?
    //consult this http://api.jquery.com/append/
    $('#section-2').css("background-color", randomColor());
    
});


var randomColor = function(){
	var colors = ["pink", "yellow", "brown", "gray", "red", "green"];
	var selected = colors[getRandomArbitrary(0, colors.length)]; 
	console.log(selected); 
	return selected; 
	
	
	
}

function getRandomArbitrary(min, max) {
  var r =  Math.round(Math.random() * (max - min) + min);
  return r; 
}