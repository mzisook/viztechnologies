console.log(d3);

//This section show how we can use d3 and JQuery together
//to capture user input, and inject new content into the DOM
var colorScale = d3.scale.category20();
	colorScale.domain([1,100]);

$('.dom-manipulation #generate-one').on('submit',function(e){
	e.preventDefault();
	var inputValue = $(this).find('#number').val();
	inputValue = +inputValue;
	console.log(inputValue);

	//check to see input value is valid
	if(!inputValue){
		alert('Wrong value');
		return;
	}else if(inputValue > 100 || inputValue <1){
		alert('Value out of range');
		return;
	}else{
		generateNewDiv(inputValue);
	}
});

$('.dom-manipulation #generate-many').on('submit',function(e){
	e.preventDefault();
	var inputValue = $(this).find('#number').val();
	inputValue = +inputValue;

	//check to see input value is valid
	if(!inputValue){
		alert('Wrong value');
		return;
	}else{
		for(var i = 0; i < inputValue; i++){
			generateNewDiv(Math.random()*100);
		}
	}
});

function generateNewDiv(value){
	//value is between 1 and 100
	var side = Math.sqrt(value)*20;
	d3.select('.dom-manipulation')
		.append('div')
		.attr('class','block')
		.style({
			'width': side + 'px',
			'height': side + 'px',
			'background': colorScale(value)
		})
		.on('mouseenter', onMouseEnter)
		.on('mousemove', onMouseMove)
		.on('mouseout', onMouseOut);
}

//tooltip user interaction
function onMouseEnter(){
	$('.tooltip').css({
		'opacity':100
	});
}
function onMouseMove(){
	var self = this; //"this" refers to div.block

	$('.tooltip').css({
		'top':d3.event.pageY - 50 + 'px',
		'left':d3.event.pageX + 50 + 'px'	
	})
	.html(
		'<span class="">Color: ' + $(self).css('background-color') + '</span>'
	);
};
function onMouseOut(){
	$('.tooltip').css({
		'opacity':0
	});
}