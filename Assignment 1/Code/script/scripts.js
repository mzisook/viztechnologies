
$(document).on("ready", function() {

	var color = "white"; 
	
	$(document).bind('mousemove',function(e){ 
	        moveTooltip(e.pageX, e.pageY); 
	        
	}); 
	
	var moveTooltip = function(x, y){
		$(".tooltip").css("top", y);
		$(".tooltip").css("left", x);
		
	}
	
	//$('#').on('click',function(e){
	//	console.log("clicked on color-transition"); 
	//    //how to add a div to div#section-1?
	//    //consult this http://api.jquery.com/append/
	//    $('#section-2').css("background-color", randomColor());
	//    
	//});
	
	$("#image-checkbox").on("change", function(e) {
		if($('#image-checkbox').prop("checked")) {
		    $("#piglet").show();
		} 
		else {
		    $("#piglet").hide();
		}
	});
	
	
	$("#submit-btn").on("click", function(e) {
		var message = $("#message-input").val(); 
		console.log(message); 
		$("#user-message").html("<p id='user=message'>" + message + "</p>"); 
		
		
	});

	$(".color-radio").on("change", function(e) {
		color = $('input[name=color]:radio:checked').val();
		$(".canvas").css("background-color", color); 
	});
	
			
		
	
	
	
});
