var ready = false;

function checkWhatIsPressed() {
	$("#ready-alert").addClass("hidden");
	$("#ready-alert").removeClass("visible");
	
	$("#beet").on("click", toggle);
	$("#carrot").on("click", toggle);
	$("#corn").on("click", toggle);
	$("#pepper").on("click", toggle);
	$("#pork").on("click", toggle);
	$("#chicken").on("click", toggle);
	$("#cow").on("click", toggle);

	$("#plate").on("click", togglePlate);
	
	$(".modal-toggle").on("click", openFarmPopup);
	$(".popup-close").on("click", closePopup);
	 
	$("#pay-button").on("click", openPayPopup);
	
	
	}
	
	
function toggle(e){
	console.log(e);
	var thisDiv = e.currentTarget;
	console.log("menu item clicked");
	if ($(thisDiv).hasClass("selected")){
		$(thisDiv).addClass("not-selected");
		$(thisDiv).removeClass("selected");
	}
	
	else if ($(thisDiv).hasClass("not-selected")){
		$(thisDiv).addClass("selected");
		$(thisDiv).removeClass("not-selected");
	
	}
}


function openFarmPopup(){
	$("#popup").addClass("visible");
	$("#popup").removeClass("hidden");
	
	$("#popup-window-menu").addClass("show");
	$("#popup-window-menu").removeClass("hide");
	
	$("#popup-window-pay").addClass("hide");
	$("#popup-window-pay").removeClass("show");
}

function openPayPopup(){
	console.log("pay clicked");
	
	
	$("#popup-window-pay").addClass("show");
	$("#popup-window-pay").removeClass("hide");
	
	$("#popup-window-menu").addClass("hide");
	$("#popup-window-menu").removeClass("show");
	
	$("#popup").addClass("visible");
	$("#popup").removeClass("hidden");
	
}


function closePopup(){
	$("#popup").addClass("hidden");
	$("#popup").removeClass("visible");
}
	







function togglePlate(){
	
	console.log("plate clicked"); 
	
	
	if (ready == false){
		$("#plate").html("<img src='img/ready.svg' width='70' height='70'>");
		$("#ready-alert").addClass("visible");
		$("#ready-alert").removeClass("hidden");
		ready = true;
	}
	
	else if (ready == true){
		$("#plate").html("<img src='img/thinking.svg' width='70' height='70'>");
		ready = false;
	}
	

}


