//Assignment 2
//Due Wednesday October 15 at 5:00PM

//Determine the size of the plot, as well as the margins
//This part is already done, but you should feel free to tweak these margins
var margin = {t:100,r:100,b:200,l:100},
    width = $('.canvas').width() - margin.l - margin.r,
    height = $('.canvas').height() - margin.t - margin.b;

//Set up SVG drawing elements -- already done
var svg = d3.select('.canvas')
    .append('svg')
    .attr('width', width + margin.l + margin.r)
    .attr('height', height + margin.t + margin.b)
    .append('g')
    .attr('transform','translate('+margin.l+','+margin.t+')');
    
 

/* Task 2.1
 * Load data using d3.csv()
 * Consult documentation here: https://github.com/mbostock/d3/wiki/CSV
 * recall the syntax is d3.csv(url[, accessor][, callback]), where assessor and callback are both functions
 * YOU WILL NEED TO WRITE THE ACCESSOR FUNCTION
 *
 * HINT:
 * d3.csv("data/world_bank_2010.csv", function(d){ ... }, function(error, rows){ ... }
 *
 *
 */
 
 
 var maxGdpPerCap, minGdpPerCap; 
 var maxTest; 
 
 var data;
 var dataRequest = d3.csv("data/world_bank_2010.csv").row(function(d){
  //array of row objects you returned
      //console.log(d);
      return {
              country: d["Country"],
              gdpPerCap : +d["GDP per capita (constant 2005 US$)"],
              //gdpPerCap : (d["GDP per capita (constant 2005 US$)"]==“..”) ? undefined : +d["GDP per capita (constant 2005 US$)"],
              healthExp: +d["Health expenditure, total (% of GDP)"]
              //more…
 
          };
  }).get(function(error, rows){
    maxGdpPerCap = d3.max(rows, function(d){
       return d.gdpPerCap;
       });
	minGdpPerCap = d3.min(rows, function(d){
      return d.gdpPerCap;
      });
	data = rows;
	
	$("#scatter-plot").on("click", function(){  
		drawScatterPlot(); 
	});
	
	$("#bar-plot").on("click", function(){  
		drawBarPlot(); 
	});
	

	                     
  });
 
 
 var drawBarPlot = function() {
  	

 	 
 	var yScale = d3.scale.linear()
 	 	    .domain([d3.min(data, function(d) { return d.healthExp; }), d3.max(data, function(d) { return d.healthExp; })])
 	 	    .range([0, height]);
 	
 	
 	var yAxis = d3.svg.axis()
 	    .scale(yScale)
 	    .orient("left")
 	    ;
 	
 	/*
 	var tip = d3.tip()
 	  .attr('class', 'd3-tip')
 	  .offset([-10, 0])
 	  .html(function(d) {
 	    return "<strong>Frequency:</strong> <span style='color:red'>" + data.country + "</span>";
 	  })
 	  	    
 	 */
 	  	    
    	
  	  svg.append("g")
  	      .attr("class", "y axis")
  	      .call(yAxis)
  	    .append("text")
  	      .attr("transform", "rotate(-90)")
  	      .attr("y", -60)
  	      .attr("dy", ".71em")
  	      .attr("fill", "red") 
  	      .style("text-anchor", "end")
  	      .text("Health Expenditure");
  	      

  	        svg.selectAll("div")
  	          .data(data)
  	        .enter().append("div")
  	          .style("width", function(d) { return d * 10 + "px"; })
  	          .text(function(data) { return data.country; });
  	  
  	
  	    
  	    
  	//svg.call(tip);
  	
  
 }
 
 
 var drawScatterPlot = function() {
 	
 	
	var xScale = d3.scale.linear()
	 	    .domain([d3.min(data, function(d) { return d.gdpPerCap; }), d3.max(data, function(d) { return d.gdpPerCap; })])
	 	    .range([0, width]);
	 
	var yScale = d3.scale.linear()
	 	    .domain([d3.min(data, function(d) { return d.healthExp; }), d3.max(data, function(d) { return d.healthExp; })])
	 	    .range([0, height]);
	
	var xAxis = d3.svg.axis()
	    .scale(xScale)
	    .orient("bottom");
	
	var yAxis = d3.svg.axis()
	    .scale(yScale)
	    .orient("left")
	    ;
	
	/*
	var tip = d3.tip()
	  .attr('class', 'd3-tip')
	  .offset([-10, 0])
	  .html(function(d) {
	    return "<strong>Frequency:</strong> <span style='color:red'>" + data.country + "</span>";
	  })
	  	    
	 */
	  	    
 	svg.append("g")
 	      .attr("class", "x axis")
 	      .attr("transform", "translate(0," + height + ")")
 	      .call(xAxis)
 	     .append("text")
 	       .attr("y", 6)
 	       .attr("x", 60)
 	       .attr("dy", ".71em")
 	       .attr("fill", "red") 
 	       .style("text-anchor", "end")
 	       .text("GDP (Per Capita)");
 	
 	  svg.append("g")
 	      .attr("class", "y axis")
 	      .call(yAxis)
 	    .append("text")
 	      .attr("transform", "rotate(-90)")
 	      .attr("y", -60)
 	      .attr("dy", ".71em")
 	      .attr("fill", "red") 
 	      .style("text-anchor", "end")
 	      .text("Health Expenditure");
 	  
 	
 	svg.selectAll('circle')
 	    .data(data)
 	    .enter()
 	    .append('circle')
 	    .attr('class','point')
 	    .attr('cx', function(data){  
 	        return xScale(data.gdpPerCap);
 	    })
 	    .attr('cy', function(data){
 	       return yScale(data.healthExp);
 	    })
 	    .attr('r', 4)
 	   .append("text")
 	     .attr("y", 6)
 	     .text(data.country)
 	    ;
 	    
 	    
 	//svg.call(tip);
 	
 
}


/* Task 2.2
 * Once data is loaded, call the draw() function
 * YOU WILL NEED TO WRITE THE DRAW FUNCTION
 * The draw() function expects one argument, which is the array object representing the full dataset
 *
 * HINT:
 * function draw(rows){ ... }
 *
 */


/* Task 2.3
 * Mapping domain to range
 * Consult the readme document
 *
 */

/* Task 2.4
 *
 * Draw axes
 * Consult the readme document
 *
 */